# Powershell Notes


## My powershell profile

```powershell
# Turn off the error sound
Set-PSReadLineOption -BellStyle None

# Show all possible completions on tab
Set-PSReadlineOption -EditMode Emacs

# Set a custom prompt
function prompt {
  $lastResult = Invoke-Expression '$?'
  if (!$lastResult) {
          Write-Host "`r`nLast command exited with error status." -ForegroundColor Red
  }
  Write-Output "${msg}`r`n$(
    Get-Location)`r`n> "
}

New-Alias -name "vq" "C:\tools\Neovim\bin\nvim-qt.exe"
New-Alias -name "v" "C:\tools\Neovim\bin\nvim.exe"
New-Alias -name "touch" "New-Item"
```