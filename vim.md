# Vim notes

## Movements

Jump to beginning of previous word: ```b```

Jump to beginning of next word: ```w```

Jump to end of next word: ```e```

Jump onto a letter in current line: ```f<letter>```

Jump up to a letter in current line: ```t<letter>```

Jump to the end of closing item (ex: parathensis): ```%```

Jump to next/previous blank line: ```{``` and ```}```

Go to beginning of file: ```gg``

Go to end of file: ```G```

Go to previous position/location: ```Ctrl + o```

Go to next position: ```Ctrl + i```

## Editing

Insert at current character: ```i```

Insert at beginning of current line: ```I```

Insert after current character ```a```

Insert at end of current line: ```A```

Replace current character: ```r```

Delete all text after cursor: ```D```

Delete all text in a line: ```0D```

Undo: ```u```

Cut current line: ```D```

Built-in auto-completion: ```ctrl + n``` or ```ctrl + p```

## Misc

Yank to system clipboard: ```"+y```

Paste from system clipboard: ```"+p```

Close a split: ```Ctrl + w, q```

Switch tabs: ```Ctrl + w or h or k or l```

List buffers: ```:ls```

Switch buffers: ```:bn```, ```:bp```, or ```Ctrl + 6```

Delete buffer: ```:bd```

Changing vim's "current directory": ```:cd <dir>```

## Help

Command: ```:h <command>```

Mapping keys: ```:h map-overview``` and ```:help map.txt```

Navigation: ```:h navigation```

## Tips

Set ```<leader>``` to something and use it for your own mappings.

File explorer: ```:Lex```

## Examples

Delete up to ")": ```dt)```

## My init.vim for Neovim on windows

```vim
" Reminder: :PlugInstall, :PlugClean
call plug#begin(stdpath('data') . '/plugged')

Plug 'dart-lang/dart-vim-plugin'
" Plug 'natebosch/vim-lsc'
" Plug 'natebosch/vim-lsc-dart'

call plug#end()

let dart_html_in_string=v:true
let g:dart_style_guide = 2
let g:dart_format_on_save = 1

colorscheme desert
set number
```

## Plugins to check out

vim-dev-icons

syntax highlighting for better-than-built-in highlighting

