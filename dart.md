# Dart notes


## Vim

(Dart-Vim-Plug)[https://github.com/dart-lang/dart-vim-plugin]

```vim
" Using vim-plug
Plug 'dart-lang/dart-vim-plugin'
Plug 'natebosch/vim-lsc'
Plug 'natebosch/vim-lsc-dart'

let dart_html_in_string=v:true
let g:dart_style_guide = 2
let g:dart_format_on_save = 1
```
