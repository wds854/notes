# Windows notes

## Context menu stuff

[How To Geek: Removing Items](https://www.howtogeek.com/howto/windows-vista/how-to-clean-up-your-messy-windows-context-menu/)
[How To Geek: Adding Items](https://www.howtogeek.com/107965/how-to-add-any-application-shortcut-to-windows-explorers-context-menu/)

Context menu locations for files and folders:
```
HKEY_CLASSES_ROOT\*\shell
HKEY_CLASSES_ROOT\*\shellex\ContextMenuHandlers
HKEY_CLASSES_ROOT\AllFileSystemObjects\ShellEx
```

Context menu locations for just folders:
```
HKEY_CLASSES_ROOT\Directory\shell
HKEY_CLASSES_ROOT\Directory\shellex\ContextMenuHandlers
```

### Hiding an option

#### For "shell" options

Add a "LegacyDisable" key to that option.

#### For "shellex" options

Edit the "Default" key, making it invalid. 
Keep the original value intact though, so you can re-enable it later if desired.

### Only show with shift

#### For "shell" options

Add an "Extended" key to that option.

#### For "shellex" options

You are SOL.

### Context menu locations for specific file types (with .xlsx as an example):

Find the "Default" value for the file extension ("Excel.Sheet.12" in this case)
```
HKEY_CLASSES_ROOT\.xlsx
```

Go to that location
```
HKEY_CLASSES_ROOT\Excel.Sheet.12\shell
```

Use the same techniques as above

### Adding an nvim-qt option

Adding context menu item for nvim-qt:
```
[HKEY_CLASSES_ROOT\*\shell\Neovim]
@="Neovim"

[HKEY_CLASSES_ROOT\*\shell\Neovim\command]
@="\"C:\\tools\\Neovim\\bin\\nvim-qt.exe\" %1"

[HKEY_CLASSES_ROOT\Directory\shell\Neovim]
@="Neovim"

[HKEY_CLASSES_ROOT\Directory\shell\Neovim\command]
@="\"C:\\tools\\Neovim\\bin\\nvim-qt.exe\" %1"

[HKEY_CLASSES_ROOT\Directory\Background\shell\Neovim]
@="Neovim"

[HKEY_CLASSES_ROOT\Directory\Background\shell\Neovim\command]
@="\"C:\\tools\\Neovim\\bin\\nvim-qt.exe\" \"%V\""
```
